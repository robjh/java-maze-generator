
SOURCES = $(wildcard src/*.java)
CLASSES = $(patsubst src/%.java,%.class,$(SOURCES))

JAR = maze.jar
EXEC = maze

.PHONY: clean run

${EXEC}: ${JAR}
	cat stub.sh $^ > $@ && chmod +x $@

${JAR}: $(CLASSES)
	jar cvmf MANIFEST.MF $@ $^

$(CLASSES): $(SOURCES)
	javac -d . $^

clean:
	@rm -v ${EXEC} ${JAR} ${CLASSES}

run: ${EXEC}
	./$^


public class Main {
	public static void main(String[] args) {
		GridBase grid = new GridSquare(10,10);

		GeneratorBase gen = new GeneratorDfs();
		gen.generate(grid);

		SolverBase sol = new SolverDfs();
		sol.solve(grid);

		grid.print();
	}
}


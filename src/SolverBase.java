public abstract class SolverBase {
	public abstract void solve(GridBase grid);
}

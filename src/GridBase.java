import java.util.ArrayList;

public abstract class GridBase {

	protected static final int VISITED    = 0x01000000;
	protected static final int BREADCRUMB = 0x02000000;
	protected static final int GOAL       = 0x04000000;

	protected int[] m_cells;

	protected int m_directions;
	protected int m_directionsCount;

	protected GridBase(int length) {
		m_cells = new int[length];
	}

	public void wallSet(int index, int direction) {
		m_cells[index] |= direction;
		index = go(index, direction);
		m_cells[index] |= opositeDirection(direction);
	}

	public void wallBreak(int index, int direction) {
		m_cells[index] &= ~direction;
		index = go(index, direction);
		m_cells[index] &= ~opositeDirection(direction);
	}

	public int opositeDirection(int direction) {
		int halfDirs = m_directionsCount / 2;
		if (direction <= (0x01 << halfDirs-1)) {
			return direction << halfDirs;
		}
		return direction >> halfDirs;
	}

	public int unvisitedNeighbours(int index, boolean respectwalls) {
		int r = 0;
		int mask = respectwalls ? ~m_cells[index] : m_directions;
		for (int i = 0 ; i < m_directionsCount ; ++i) {
			try {
				int direction = 0x01 << i;
				if (
					(mask & direction) > 0 &&
					(m_cells[go(index, direction)] & VISITED) == 0
				) {
					r |= direction;
				}
			}
			catch (ArrayIndexOutOfBoundsException e) {}
		}
		return r;
	}

	public void visit(int index) {
		m_cells[index] |= VISITED;
	}

	public void breadcrumb(int index) {
		m_cells[index] |= BREADCRUMB;
	}

	public void goal(int index) {
		m_cells[index] |= GOAL;
	}

	public boolean is_goal(int index) {
		return (m_cells[index] & GOAL) > 0;
	}

	public void clean() {
		for (int i = 0 ; i < m_cells.length ; ++i) {
			m_cells[i] &= m_directions;
		}
	}

	public int directionsCount() {
		return m_directionsCount;
	}

	public int size() {
		return m_cells.length;
	}

	public abstract int go(int index, int direction);

	public abstract void print();

}

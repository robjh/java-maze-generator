public abstract class GeneratorBase {
	public abstract void generate(GridBase grid);
}

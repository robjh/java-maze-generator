import java.util.Stack;
import java.util.Random;

public class GeneratorDfs extends GeneratorBase {


	public void generate(GridBase grid) {

		Stack<Integer> stack = new Stack<Integer>();
		int[] choices = new int[grid.directionsCount()];

		stack.push(grid.size()-1);
		grid.visit(grid.size()-1);
		Random r = new Random();

		while (stack.size() > 0) {
			// if there are unvisited neighbours,
			//    choose a random direction,
			//    break the wall, 
			//    make the neighbour head of the stack.
			// else, pop the stack.
			int current = stack.peek();
			int unvisited = grid.unvisitedNeighbours(current, false);

			if (unvisited > 0) {
				int choices_count = 0;
				for (int i = 0 ; i < grid.directionsCount() ; ++i) {
					if (((0x01 << i) & unvisited) > 0) {
						choices[choices_count++] = 0x01 << i;
					}
				}
				int nextDir = choices[r.nextInt(choices_count)];
				grid.wallBreak(current, nextDir);
				current = grid.go(current,nextDir);
				grid.visit(current);
				stack.push(current);
			} else {
				stack.pop();
			}
		}

		grid.clean();
		grid.goal(grid.size()-1);

	}
}

import java.util.Arrays;

public class GridSquare extends GridBase {

	private static final int LEFT  = 0x01;
	private static final int UP    = 0x02;
	private static final int RIGHT = 0x04;
	private static final int DOWN  = 0x08;
	private Coordinate m_size;

	public GridSquare(int x, int y) {
		super(x * y);

		m_directions = LEFT | UP | RIGHT | DOWN;
		m_directionsCount = 4;
		m_size = new Coordinate(x, y);

		Arrays.fill(m_cells, m_directions);
	}

	private int coord2index(Coordinate c) {
		return c.y() * m_size.x() + c.x();
	}
	private Coordinate index2coord(int index) {
		return new Coordinate(
			index % m_size.x(),
			index / m_size.x()
		);
	}

	public int go(int index, int direction) {
		Coordinate c = index2coord(index);
		c = switch (direction) {
			case LEFT  -> new Coordinate(c.x() - 1, c.y()    );
			case UP    -> new Coordinate(c.x(),     c.y() - 1);
			case RIGHT -> new Coordinate(c.x() + 1, c.y()    );
			case DOWN  -> new Coordinate(c.x(),     c.y() + 1);
			default    -> throw new Illegal­Argument­Exception();
		};
		if (c.x() < 0 || c.y() < 0 || c.x() >= m_size.x() || c.y() >= m_size.y()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return coord2index(c);
	}

	public void print() {
		for (int y = 0 ; y < m_size.y() ; ++y) {
			StringBuilder line1 = new StringBuilder("+");
			StringBuilder line2 = new StringBuilder("|");
			for (int x = 0 ; x < m_size.x() ; ++x) {
				int i = coord2index(new Coordinate(x, y));
				if (0 < (m_cells[i] & GOAL)) {
					line2.append(" G");
				} else {
					line2.append(0 < (m_cells[i] & BREADCRUMB) ? " X"   : "  ");
				}
				line1.append(0 < (m_cells[i] &    UP) ? "---+" : "   +");
				line2.append(0 < (m_cells[i] & RIGHT) ?   " |" :   "  ");
			}
			System.out.println(line1);
			System.out.println(line2);
		}

		// bottom wall
		StringBuilder line = new StringBuilder("+");
		for (int x = 0 ; x < m_size.x() ; ++x) {
			line.append("---+");
		}
		System.out.println(line);
	}
}

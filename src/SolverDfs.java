import java.util.Stack;
import java.util.Random;

public class SolverDfs extends SolverBase {


	public void solve(GridBase grid) {

		Stack<Integer> stack = new Stack<Integer>();
		boolean foundGoal = false;

		stack.push(0);
		grid.visit(0);

		while (stack.size() > 0) {
			// if the goal has been found, place a breadcrumb and backtrack.
			// if there are unvisited neighbours,
			//    choose the next direction
			//    make the neighbour head of the stack.
			// else, pop the stack.

			int current = stack.peek();

			if (foundGoal) {
				grid.breadcrumb(current);
				stack.pop();
				continue;
			}
			else if (grid.is_goal(current)) {
				foundGoal = true;
				stack.pop();
				continue;
			}

			int unvisited = grid.unvisitedNeighbours(current, true);
			if (unvisited > 0) {
				int nextDir = 0;
				for (int i = 0 ; i < grid.directionsCount() ; ++i) {
					if (((0x01 << i) & unvisited) > 0) {
						nextDir = 0x01 << i;
					}
				}
				current = grid.go(current, nextDir);
				grid.visit(current);
				stack.push(current);
			} else {
				stack.pop();
			}
		}
	}
}
